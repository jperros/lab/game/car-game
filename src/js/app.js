import '../css/style.css';
import { init, GameLoop, initKeys } from 'kontra';
import Car from './modules/car.js';

let { canvas, context } = init();
let pixelRatio = window.devicePixelRatio;

canvas.width = window.innerWidth
canvas.height = window.innerHeight

canvas.style.width = canvas.width * `${pixelRatio}px`
canvas.style.height = canvas.height * `${pixelRatio}px`

context.scale(pixelRatio, pixelRatio)
// TODO : Make a Singleton class to manage game ressource
window.singleton = {
    imagesToLoad : {
        car : 'src/assets/images/car--blue.png'
    },
    images: {}
}

for (const [name, src] of Object.entries(window.singleton.imagesToLoad)) {
    let image = new Image();
    image.src = src;
    image.onload = function() {
        loadRessources(name, image)
    }
}


// Waiting all ressource loading before start the gameloop
function loadRessources(name, image) {
    console.log('new ressrouce loaded', image);
    window.singleton.images[name] = image
    if(Object.keys(window.singleton.images).length == Object.keys(window.singleton.imagesToLoad).length) {
        initGameLoop()
    }
}

// let sprite = Sprite({
//     x: 20,
//     y: 20,
//     width: 14,
//     height: 14,
//     dx: 1,
//     dy: 1,
//     color: 'green',
//     update: function() {
//         self.direction = keyPressed('right') - keyPressed('left')
//         self.velocity = 0
//     },
//     move: function (direction, acceleration) {
//         self.velocity = 0
//     }
// })


function initGameLoop() {
    let car = new Car({
        x: 20,
        y: 20,
        width: 100,
        height: 100
    });
    initKeys();
    let loop = GameLoop({
        update: () => {
            // sprite.update()
            car.update()
        },
        render: () => {
            // sprite.render()
            car.render()
        }
    })
    loop.start()
}
