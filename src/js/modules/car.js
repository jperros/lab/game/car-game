import { GameObject, keyPressed, Vector, degToRad, Sprite } from 'kontra';

/**
 * @class Car
 * @extends GameObject
 * 
 * @param {Object} [props] - The properties of the car.
 * @param {number} [props.speed = 0] - The speed of the car.
 */
class Car extends GameObject.class {
    init({ speed = 0, ...props } = {}) {
        // TODO: Add object specific properties here
        super.init({ speed, ...props });
        this.children = [
            Sprite({
                anchor: {x: 0.5, y: 0.5},
                image: window.singleton.images.car
            })
        ]
    }

    render() {
        super.render()
        for(let child of this.children) {
            child.render();
        }
    }
   
    update() {
        super.update()
        this.handleKeyboard();
        for(let child of this.children) {
            child.update();
        }
    }

    handleKeyboard() {
        let direction = Number(keyPressed('right')) - Number(keyPressed('left'))
        let velocityKey = Number(keyPressed('up')) - Number(keyPressed('down'))

        this.move(direction, velocityKey)
    }

    move(direction, velocityKey) {
        let angle = degToRad((direction + 1) * 90)
        direction = Vector(Math.cos(angle), Math.sin(angle))
        this.velocity = direction.scale(velocityKey * this.speed)
    }
}

export default Car;