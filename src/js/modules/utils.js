export const loadImage = (url) => {
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.onload = () => resolve(img);
        img.onerror = () => reject(new Error(`Unable to load image at ${url}`));
        img.src = url;
    });
}